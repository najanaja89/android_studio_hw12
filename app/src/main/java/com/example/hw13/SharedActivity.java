package com.example.hw13;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class SharedActivity extends AppCompatActivity {

    private final String KEY_NAME = "KEY_NAME";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shared);

        SharedPreferences sharedPreferences = getSharedPreferences("My", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(KEY_NAME, "Ruslan");
        editor.commit();


        String name = sharedPreferences.getString(KEY_NAME, "Karina");
        Toast.makeText(SharedActivity.this, name, Toast.LENGTH_LONG).show();

        Button open = findViewById(R.id.openBtn);
        open.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SharedActivity.this, SettingsActivity.class));
            }
        });
    }
}