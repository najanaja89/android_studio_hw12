package com.example.hw13;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;

public class MainActivity extends AppCompatActivity {
    private int REQUEST_CODE = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button button = findViewById(R.id.preferenceBtn);

        SharedPreferences sharedPreferences = getSharedPreferences("My", MODE_PRIVATE);
        String name = sharedPreferences.getString("KEY_NAME", "");
        Toast.makeText(MainActivity.this, name, Toast.LENGTH_LONG).show();

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, SharedActivity.class);
                startActivity(intent);
            }
        });


        writeToFile("myfile.txt");
        readFromFile("myfile.txt");

        if (isExternalStorageWritable()) {
            Log.d("Hello", "External Storage Writeable");
        } else if (isExternalStorageReadable()) {
            Log.d("Hello", "External Storage Readable");
        }

        ActivityCompat.requestPermissions(MainActivity.this,
                new String[]{
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                },
                REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODE) {
            boolean isGranted = false;
            for (int result : grantResults) {
                if (result == PackageManager.PERMISSION_GRANTED) {
                    isGranted = true;
                } else if (result == PackageManager.PERMISSION_DENIED) {
                    isGranted = false;
                }
            }
            if(isGranted){
                File publicDirectory = getDownloadDirectoryDir("my");
                File storageDirectory = getDownloadStorageDir("store");
                writeToExternalFile(storageDirectory.getAbsolutePath(), "file.txt");
                readFromExternalFile(storageDirectory.getAbsolutePath(), "file.txt");

            }
        }
    }

    private void readFromExternalFile(String dir, String filename){
        try {
            File file = new File(dir, filename);
            FileInputStream fileInputStream = new FileInputStream(file);
            InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            StringBuffer stringBuffer = new StringBuffer();
            String read;
            while ((read = bufferedReader.readLine()) !=null){
                stringBuffer.append(read);
            }
            Log.d("Hello", stringBuffer.toString());
        }
        catch (IOException e){
            e.printStackTrace();
        }

    }

    private void writeToExternalFile (String dir, String filename){
        try {
            File file = new File(dir, filename);
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            PrintWriter printWriter = new PrintWriter(fileOutputStream);
            printWriter.println("Hello World");
            printWriter.println("Ruslan Tyo");
            printWriter.flush();
            printWriter.close();
            fileOutputStream.close();

        }catch (IOException e){
            e.printStackTrace();
        }
    };

    private void writeToFile(String fileName) {
        try {
            FileOutputStream fileOutputStream = openFileOutput(fileName, Context.MODE_PRIVATE);
            fileOutputStream.write("Content".getBytes());
            fileOutputStream.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private void readFromFile(String filename) {
        try {
            InputStream inputStream = openFileInput(filename);
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            StringBuilder stringBuilder = new StringBuilder();
            String read;
            while ((read = bufferedReader.readLine()) != null) {
                stringBuilder.append(read).append("\n");
            }
            inputStream.close();

            Toast.makeText(MainActivity.this, stringBuilder.toString(), Toast.LENGTH_LONG).show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        return state.equals(Environment.MEDIA_MOUNTED);
    }

    private boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        return state.equals(Environment.MEDIA_MOUNTED) || state.equals(Environment.MEDIA_MOUNTED_READ_ONLY);
    }

    private File getDownloadDirectoryDir(String name) {
        File file =
                new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), name);
        if (!file.mkdir()) {
            Log.d("Hello", "Public Directory not printed");
        }
        return file;
    }

    private File getDownloadStorageDir(String name){
        File file = new File(getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS), name);
        if (!file.mkdirs()){
            Log.e("Hello", "Storage Directory not created");
        }
        return file;
    }

}